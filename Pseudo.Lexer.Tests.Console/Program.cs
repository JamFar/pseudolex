﻿using Pseudo.Lexer.Services;
using System;
using System.Security.Cryptography;

namespace Pseudo.Lexer.Tests.Console
{
    public class Program
    {
        static void Main(string[] args)
        {
            GuidService gs = new GuidService();
            var guid = gs.GenerateGUIDSeed();
            var seed = gs.GenerateGUIDSeed();

            foreach(var b in guid.ToByteArray())
                System.Console.Write(Convert.ToString(b, 2).PadLeft(8, '0') + " ");
            System.Console.WriteLine();
            foreach (var b in seed.ToByteArray())
                System.Console.Write(Convert.ToString(b, 2).PadLeft(8, '0') + " ");
            System.Console.WriteLine();

            var xored = gs.XorGuids(guid, seed);

            foreach (var b in xored.ToByteArray())
                System.Console.Write(Convert.ToString(b, 2).PadLeft(8, '0') + " ");
            System.Console.WriteLine();

            var orig = gs.XorGuids(xored, seed);

            foreach (var b in orig.ToByteArray())
                System.Console.Write(Convert.ToString(b, 2).PadLeft(8, '0') + " ");
            System.Console.WriteLine();

            System.Console.WriteLine(orig.Equals(guid));
        }

        static void GenerateUUID()
        {
            var rng = new RNGCryptoServiceProvider();
            var bytes = new byte[16];
            rng.GetBytes(bytes);
            bytes[8] = (byte)((((bytes[8] << 4) & 0xff) >> 4) + 64);
            bytes[9] = (byte)((((bytes[9] << 2) & 0xff) >> 2) + 128);
            foreach (var b in bytes)
            {
                System.Console.Write(Convert.ToString(b, 2).PadLeft(8, '0') + " ");
            }
            System.Console.WriteLine();
        }
    }
}
