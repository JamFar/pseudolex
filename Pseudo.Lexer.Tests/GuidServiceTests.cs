﻿using Pseudo.Lexer.Services;
using System;
using Xunit;

namespace Pseudo.Lexer.Tests
{
    public class GuidServiceTests
    {
        [Fact]
        public void GuidSeedsGeneratedProperly()
        {
            GuidService gs = new GuidService();
            var seed1 = gs.GenerateGUIDSeed();
            var seed2 = gs.GenerateGUIDSeed();
            Assert.False(Guid.Equals(seed1, seed2));
        }

        [Fact]
        public void XorProcedureWorks()
        {
            GuidService gs = new GuidService();
            var seed1 = gs.GenerateGUIDSeed();
            var seed2 = gs.GenerateGUIDSeed();
            var xored = gs.XorGuids(seed1, seed2);
            var orig = gs.XorGuids(xored, seed2);
            Assert.True(orig.Equals(seed1));
        }
    }
}
