﻿using Pseudo.Lexer.DataStructures;
using Pseudo.Lexer.DTOs;
using System;
using System.Collections.Generic;
using System.Reflection;
using Xunit;

namespace Pseudo.Lexer.Tests
{
    public class TransMapTests
    {
        [Fact]
        public void ExistsWorksWhenNothingInMap()
        {
            var map = new TransMap();
            Assert.False(map.Exists(new State(), 'x'));
        }
        [Fact]
        public void RegisterWorksBasically()
        {
            var fr = new State();
            var to = new State("FINAL");
            var map = new TransMap();
            map.Register(fr, 'c', to);
            Assert.True(map.Exists(fr, 'c'));
            Assert.Equal(to, map.Apply(fr, 'c'));
        }
        [Fact]
        public void DeleteWorksBasically()
        {
            State fr = new State();
            State to = new State();
            var map = new TransMap();
            map.Register(fr, 'c', to);
            Assert.True(map.Exists(fr, 'c'));
            map.Delete(fr, 'c');
            Assert.False(map.Exists(fr, 'c'));
        }
        [Fact]
        public void ClearWorks()
        {
            State fr = new State();
            State to = new State();
            var map = new TransMap();
            map.Register(fr, 'c', to);
            map.Register(fr, 'd', to);
            
            var f = typeof(TransMap).GetProperty("Map", BindingFlags.Instance | BindingFlags.NonPublic);
            var dict = (Dictionary<Tuple<State, char>, State>)f.GetValue(map);

            Assert.Equal(2, dict.Count);
            map.Clear();
            Assert.Empty(dict);
        }
        [Fact]
        public void CloneWorks()
        {
            State fr = new State();
            State to = new State();
            var map = new TransMap();
            map.Register(fr, 'c', to);
            map.Register(fr, 'd', to);

            var mapclone = map.Clone();

            var f = typeof(TransMap).GetProperty("Map", BindingFlags.Instance | BindingFlags.NonPublic);
            var dict = (Dictionary<Tuple<State, char>, State>)f.GetValue(map);
            var dictclone = (Dictionary<Tuple<State, char>, State>)f.GetValue(mapclone);

            Assert.Equal(dict.Count, dictclone.Count);
        }
    }
}
