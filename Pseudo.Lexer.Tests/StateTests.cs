using Pseudo.Lexer.DTOs;
using Xunit;

namespace Pseudo.Lexer.Tests
{
    public class StateTests
    {
        [Fact]
        public void GuidsGeneratedProperly()
        {
            var s1 = new State();
            var s2 = new State();
            Assert.NotEqual(s2.Id, s1.Id);
        }
        [Fact]
        public void FinalIsRecognised()
        {
            var s1 = new State("IAmFinal");
            var s2 = new State();
            var s3 = new State("");
            var s4 = new State();
            s4.Token = "IAmFinalToo";
            Assert.True(s1.IsFinal());
            Assert.False(s2.IsFinal());
            Assert.False(s3.IsFinal());
            Assert.True(s4.IsFinal());
            s4.Token = "";
            Assert.False(s4.IsFinal());
        }
    }
}
