﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Pseudo.Lexer.Services
{
    /// <summary>
    /// RFC4122 Compliant GUID services (https://tools.ietf.org/html/rfc4122#section-4.1.3)
    /// </summary>
    public class GuidService
    {
        RNGCryptoServiceProvider rng => new RNGCryptoServiceProvider();
        /// <summary>
        /// Sets the clock_seq_hi_and_reserved to zero and one, respectively, and the four most significant bits (bits 12 through 15) of the
        /// time_hi_and_version field to the 4-bit version number (0100) as per https://tools.ietf.org/html/rfc4122#section-4.1.3 specifications.
        /// </summary>
        /// <param name="bytes"></param>
        private void SetBitsRFC4122Compliant(byte[] bytes)
        {
            bytes[7] = (byte)((((bytes[7] << 4) & 0xff) >> 4) + 64);
            bytes[8] = (byte)((((bytes[8] << 2) & 0xff) >> 2) + 128);
        }
        /// <summary>
        /// Generates a cryptographically secure GUID as per https://tools.ietf.org/html/rfc4122#section-4.1.3 specifications.
        /// Intended use is to seed the creation of another Guid, given a Microsoft-compliant Guid, using the XORGuids(..) method.
        /// </summary>
        /// <returns></returns>
        public Guid GenerateGUIDSeed()
        {
            var bytes = new byte[16];
            rng.GetBytes(bytes);
            SetBitsRFC4122Compliant(bytes);
            return new Guid(bytes);
        }
        /// <summary>
        /// XORs two Guids to create a new valid Guid (reversible) as per https://tools.ietf.org/html/rfc4122#section-4.1.3 specifications.
        /// </summary>
        /// <param name="active">The Guid to be XOR'd</param>
        /// <param name="seed">The seed Guid</param>
        /// <returns></returns>
        public Guid XorGuids(Guid active, Guid seed)
        {
            byte[] ret = new byte[16];
            var activebytes = active.ToByteArray();
            var seedbytes = seed.ToByteArray();
            for(int i=0; i<16; i++)
                ret[i] = (byte)(activebytes[i] ^ seedbytes[i]);
            SetBitsRFC4122Compliant(ret);
            return new Guid(ret);
        }
    }
}
