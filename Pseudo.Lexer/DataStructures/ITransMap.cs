﻿using Pseudo.Lexer.DTOs;

namespace Pseudo.Lexer.DataStructures
{
    public interface ITransMap
    {
        State Apply(State from, char transition);
        void Register(State from, char transition, State to);
        void Delete(State from, char transition);
        bool Exists(State from, char transition);
        void Clear();
    }
}
