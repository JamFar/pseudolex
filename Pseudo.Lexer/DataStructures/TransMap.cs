﻿using Pseudo.Lexer.DTOs;
using System;
using System.Collections.Generic;

namespace Pseudo.Lexer.DataStructures
{
    public class TransMap : ITransMap, ICloneable
    {
        private Dictionary<Tuple<State, char>, State> Map { get; set; }
        public TransMap() => Map = new Dictionary<Tuple<State, char>, State>();
        public State Apply(State from, char transition) => Map[new Tuple<State, char>(from, transition)];
        public void Delete(State from, char transition) => Map.Remove(new Tuple<State, char>(from, transition));
        public bool Exists(State from, char transition) => Map.ContainsKey(new Tuple<State, char>(from, transition));
        public void Register(State from, char transition, State to) => Map[new Tuple<State, char>(from, transition)] = to;
        public void Clear() => Map.Clear();

        public object Clone()
        {
            var IdToNewState = new Dictionary<Guid, State>();
            var ret = new TransMap();
            foreach(var tuple in Map)
            {
                State frstate = tuple.Key.Item1;
                State tostate = tuple.Value;
                if (!IdToNewState.ContainsKey(frstate.Id))
                    IdToNewState[frstate.Id] = (State)frstate.Clone();
                if (!IdToNewState.ContainsKey(tostate.Id))
                    IdToNewState[tostate.Id] = (State)tostate.Clone();
            }
            foreach(var tuple in Map)
            {
                Guid frid = tuple.Key.Item1.Id;
                Guid toid = tuple.Value.Id;
                ret.Register(IdToNewState[frid], tuple.Key.Item2, IdToNewState[toid]);
            }
            return ret;
        }
    }
}
