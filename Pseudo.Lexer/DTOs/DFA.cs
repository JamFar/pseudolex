﻿using Pseudo.Lexer.DataStructures;
using System;

namespace Pseudo.Lexer.DTOs
{
    public class DFA : ICloneable
    {
        public ITransMap Map { get; private set; }
        public State Start { get; set; }
        public DFA(ITransMap dependency) => Map = dependency;

        public object Clone()
        {
            /*
             *  clone start into startclone  
             *  find start in Map
             *  set start to startclone
             *  clone Map into MapClone
             *  
             *  
             */
            return null;
        }
    }
}
