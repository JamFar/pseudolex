﻿using System;

namespace Pseudo.Lexer.DTOs
{
    public class State : ICloneable
    {
        public Guid Id { get; set; }
        public string Token { get; set; }
        public bool IsFinal() => !Token.Equals("");

        public object Clone() => new State(Token);

        public State(string Token = "")
        {
            Id = Guid.NewGuid();
            this.Token = Token;
        }        
    }
}
